image: odoo:13

services:
    - postgres:11

variables:
    POSTGRES_DB: ${CI_PROJECT_PATH_SLUG}_${CI_COMMIT_REF_SLUG}
    POSTGRES_USER: odoo
    POSTGRES_PASSWORD: odoo
    DOCKER_IMAGE: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}
    ENVIRONMENT_PROJECT_URL: ${CI_PROJECT_PATH_SLUG}.${TESTING_SERVER_DOMAIN}
    ENVIRONMENT_STAGING_PROJECT_URL: ${CI_PROJECT_PATH_SLUG}.${STAGING_SERVER_DOMAIN}
    NGINX_CONFIG: |
        upstream docker_{{ ENVIRNONMENT_URL }} {
            server {{ CONTAINER_URL }} max_fails=0 fail_timeout=12000s;
        }
        server {
                listen                      80;
                server_name                 {{ ENVIRNONMENT_URL }};
                charset                     utf-8;
                client_max_body_size        0;
                chunked_transfer_encoding   on;

                access_log  /var/log/nginx/testing_{{ ENVIRNONMENT_URL }}_access.log;
                error_log /var/log/nginx/testing_{{ ENVIRNONMENT_URL }}_error.log;

                location / {
                        proxy_buffering         off;
                        proxy_set_header        Host            $$http_host;
                        proxy_set_header        X-Real-IP       $$remote_addr;
                        proxy_set_header        X-Forward-For   $$proxy_add_x_forwarded_for;
                        proxy_set_header        X-Scheme        $$scheme;
                        proxy_pass              http://docker_{{ ENVIRNONMENT_URL }};
                        proxy_connect_timeout   12000s;
                        proxy_read_timeout      12000s;
                }
        }
    # modules to be tested and installed manually (they're not part of this
    # repository and not dependencies)
    MANUAL_MODULES: ''

stages:
    - build
    - test
    - review
    - install
    - release
    - deploy
    - cleanup

before_script:
    # get modules from the current repository
    - |
        export ODOO_MODULES=$(find -maxdepth 2 -name '__openerp__.py' -o \
                              -name '__manifest__.py' -o -name '__odoo__.py' \
                              -type f | cut -f 2 -d '/' | tr '\n' ',' | rev | \
                              cut -c 2- | rev)
    # add manual modules for Dockerfile external modules (to be tested and
    # installed automatically if they're not dependencies)
    - |
        if [[ "$MANUAL_MODULES" != "" ]]; then
            export ODOO_MODULES="$ODOO_MODULES,$MANUAL_MODULES";
        fi
    # safe fallback to base module
    - |
        if [[ "$ODOO_MODULES" == "" ]]; then
            export ODOO_MODULES="base";
        fi
    - export PGHOST=$POSTGRES_PORT_5432_TCP_ADDR
    - export PGPORT=$POSTGRES_PORT_5432_TCP_PORT
    - export PGUSER=$POSTGRES_ENV_POSTGRES_USER
    - export PGPASSWORD=$POSTGRES_ENV_POSTGRES_PASSWORD

build:docker:
    stage: build
    script:
        - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
        - docker build --pull . -t $DOCKER_IMAGE
        - docker push $DOCKER_IMAGE
    tags:
        - shell
        - testing
        - compute
    only:
        - test-standalone-dependencies
        - /^feature\/.*/
        - /^hotfix\/.*/
        - develop
        - "13.0"
        - master

test:modules:
    stage: test
    image: $DOCKER_IMAGE
    dependencies:
        - build:docker
    script:
        # run without dependencies, some of them fail in the odoo level
        - /usr/bin/odoo --stop-after-init --database $POSTGRES_DB --init $ODOO_MODULES
    artifacts:
        paths:
            - .coverage
            - htmlcov
    coverage: '/TOTAL\s+\d+\s+\d+\s+(\d+)%/'
    tags:
        - docker
        - testing
        - compute
    only:
        - /^feature\/.*/
        - /^hotfix\/.*/
        - develop

test:dependencies:
    stage: test
    image: $DOCKER_IMAGE
    dependencies:
        - build:docker
    script:
        - /usr/bin/odoo --stop-after-init --test-enable --database $POSTGRES_DB --init $ODOO_MODULES
    allow_failure: true
    tags:
        - docker
        - testing
        - compute
    only:
        - /^feature\/.*/
        - /^hotfix\/.*/

test:dependencies:standalone:
    stage: test
    image: $DOCKER_IMAGE
    dependencies:
        - build:docker
    allow_failure: true
    script:
        - export TOTAL_MODULES_NUM=${#ODOO_MODULES[@]}
        - export ROUND_NUM=$(($TOTAL_MODULES_NUM / 4))
        - export LOOP_COUNT=0
        # run 5 concurrent tasks in parallel to speed up the test
        - |
            (while [ $LOOP_COUNT -lt $TOTAL_MODULES_NUM ]; do
                (for MODULE in ${ODOO_MODULES[@]:$LOOP_COUNT:$ROUND_NUM}; do \
                     echo "==================================================="
                     echo "Testing ${MODULE} on ${POSTGRES_DB}_${MODULE} ..."
                     echo "==================================================="
                     /usr/bin/odoo --stop-after-init --database \
                         ${POSTGRES_DB}_${MODULE} --init $MODULE \
                         --log-level=warn || echo "FAILED Testing ${MODULE}."
                     echo "==================================================="
                     echo "Dropping ${POSTGRES_DB}_${MODULE} ..."
                     echo "==================================================="
                     dropdb ${POSTGRES_DB}_${MODULE}
                done) > test_${LOOP_COUNT}.txt 2>&1 &
                export LOOP_COUNT=$(($LOOP_COUNT + $ROUND_NUM))
                # wait for previous tasks to finish
                if [ $LOOP_COUNT -ge $TOTAL_MODULES_NUM ]; then
                    wait `jobs -p`
                fi
            done)
        - cat test_*.txt
    tags:
        - docker
        - testing
        - compute
    only:
        # tag a commit with this keyword and push it to trigger this test
        - test-standalone-dependencies

test:all:
    stage: test
    image: $DOCKER_IMAGE
    dependencies:
        - build:docker
    script:
        - /usr/bin/odoo --stop-after-init --test-enable --database $POSTGRES_DB --init all
    allow_failure: true
    tags:
        - docker
        - testing
        - compute
    only:
        - develop

deploy:review:
    stage: review
    dependencies:
        - build:docker
    environment:
        name: $CI_COMMIT_REF_NAME
        url: http://$CI_ENVIRONMENT_SLUG.$CI_PROJECT_PATH_SLUG.$TESTING_SERVER_DOMAIN
        on_stop: delete:review
    script:
        - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
        # Pull latest images (if built on different server) and don't build
        # again
        - docker pull $DOCKER_IMAGE
        - docker-compose -p ${CI_PROJECT_PATH_SLUG}_${CI_COMMIT_REF_SLUG} up -d
        - export CONTAINER_URL="`docker-compose -p ${CI_PROJECT_PATH_SLUG}_${CI_COMMIT_REF_SLUG} port odoo 8069`"
        - export ENVIRNONMENT_URL="${CI_ENVIRONMENT_SLUG}.${ENVIRONMENT_PROJECT_URL}"
        - |
            echo "$NGINX_CONFIG" | \
            sed -e "s!{{ ENVIRNONMENT_URL }}!${ENVIRNONMENT_URL}!g" -e "s!{{ CONTAINER_URL }}!${CONTAINER_URL}!g" | \
            sudo tee /etc/nginx/conf.d/testing_${ENVIRNONMENT_URL}.conf > /dev/null
        - "sudo nginx -t && sudo systemctl reload nginx.service"
    tags:
        - shell
        - testing
        - deploy
    only:
        - /^feature\/.*/
        - /^hotfix\/.*/

delete:review:
    stage: review
    variables:
        GIT_STRATEGY: none
    environment:
        name: $CI_COMMIT_REF_NAME
        action: stop
    when: manual
    script:
        - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
        - docker-compose -p ${CI_PROJECT_PATH_SLUG}_${CI_COMMIT_REF_SLUG} down -v --remove-orphans --rmi all
        - export ENVIRNONMENT_URL="${CI_ENVIRONMENT_SLUG}.${ENVIRONMENT_PROJECT_URL}"
        - sudo rm -f /etc/nginx/conf.d/testing_${ENVIRNONMENT_URL}.conf
        - sudo systemctl reload nginx.service
    tags:
        - shell
        - testing
        - deploy
    only:
        - /^feature\/.*/
        - /^hotfix\/.*/

install:review:demo:
    stage: install
    dependencies:
        - deploy:review
    script:
        # wait for postgresql to be up and running
        - sleep 10
        # we update current database here (if exists)
        - docker-compose -p ${CI_PROJECT_PATH_SLUG}_${CI_COMMIT_REF_SLUG} run --rm odoo -- -d ${POSTGRES_DB}-demo -i $ODOO_MODULES -u $ODOO_MODULES --stop-after-init
    tags:
        - shell
        - testing
        - deploy
    only:
        - /^feature\/.*/
        - /^hotfix\/.*/


release:docker:staging:
    stage: release
    dependencies:
        - build:docker
    script:
        - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
        - docker pull $DOCKER_IMAGE
        - docker tag $DOCKER_IMAGE ${CI_REGISTRY_IMAGE}:staging
        - docker push ${CI_REGISTRY_IMAGE}:staging
    tags:
        - shell
        - testing
        - compute
    only:
        - develop

release:docker:production:
    stage: release
    dependencies:
        - build:docker
    script:
        - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
        - docker pull $DOCKER_IMAGE
        - docker tag $DOCKER_IMAGE ${CI_REGISTRY_IMAGE}:production
        - docker tag $DOCKER_IMAGE ${CI_REGISTRY_IMAGE}:latest
        - docker push ${CI_REGISTRY_IMAGE}:production
        - docker push ${CI_REGISTRY_IMAGE}:latest
    tags:
        - shell
        - testing
        - compute
    only:
        - "10.0"
        - master

deploy:staging:
    stage: deploy
    dependencies:
        - release:docker:staging
    environment:
        name: staging
        url: http://$CI_ENVIRONMENT_SLUG.$CI_PROJECT_PATH_SLUG.$STAGING_SERVER_DOMAIN
    script:
        - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
        - docker pull "${CI_REGISTRY_IMAGE}:staging"
        - DOCKER_IMAGE="${CI_REGISTRY_IMAGE}:staging" docker-compose -p ${CI_PROJECT_PATH_SLUG}_${CI_COMMIT_REF_SLUG} up -d
        # wait for postgresql to be up and running
        - sleep 10
        # we update current database here (if exists)
        - DOCKER_IMAGE="${CI_REGISTRY_IMAGE}:staging" docker-compose -p ${CI_PROJECT_PATH_SLUG}_${CI_COMMIT_REF_SLUG} run --rm odoo -- -d $POSTGRES_DB -i $ODOO_MODULES -u base --stop-after-init
        - export CONTAINER_URL="`docker-compose -p ${CI_PROJECT_PATH_SLUG}_${CI_COMMIT_REF_SLUG} port odoo 8069`"
        - export ENVIRNONMENT_URL="${CI_ENVIRONMENT_SLUG}.${ENVIRONMENT_STAGING_PROJECT_URL}"
        - |
            echo "$NGINX_CONFIG" | \
            sed -e "s!{{ ENVIRNONMENT_URL }}!${ENVIRNONMENT_URL}!g" -e "s!{{ CONTAINER_URL }}!${CONTAINER_URL}!g" | \
            sudo tee /etc/nginx/conf.d/testing_${ENVIRNONMENT_URL}.conf > /dev/null
        - "sudo nginx -t && sudo systemctl reload nginx.service"
    tags:
        - shell
        - staging

    only:
        - develop

delete:staging:
    stage: deploy
    variables:
        GIT_STRATEGY: none
    environment:
        name: staging
        action: stop
    when: manual
    script:
        - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
        - docker-compose -p ${CI_PROJECT_PATH_SLUG}_${CI_COMMIT_REF_SLUG} down -v --remove-orphans --rmi all
        - export ENVIRNONMENT_URL="${CI_ENVIRONMENT_SLUG}.${ENVIRONMENT_STAGING_PROJECT_URL}"
        - sudo rm -f /etc/nginx/conf.d/testing_${ENVIRNONMENT_URL}.conf
        - sudo systemctl reload nginx.service
    tags:
        - shell
        - staging
    only:
        - develop

cleanup:docker:compute:
    stage: cleanup
    when: always
    script:
        # remove old stopped containers
        - 'docker rm -v $(docker ps --filter "status=exited" --filter "name=${CI_PROJECT_NAME}" -q --no-trunc) || :'
        # remove old unused images
        - 'docker rmi $(docker images -f dangling=true -q --no-trunc) || :'
        # remove old unused containers
        - 'docker volume rm $(docker volume ls -f dangling=true -q) || :'
    tags:
        - testing
        - shell
        - compute
    only:
        - test-standalone-dependencies
        - /^feature\/.*/
        - /^hotfix\/.*/
        - develop
        - "10.0"
        - master

cleanup:docker:deploy:
    stage: cleanup
    when: always
    script:
        # remove old stopped containers
        - 'docker rm -v $(docker ps --filter "status=exited" --filter "name=${CI_PROJECT_NAME}" -q --no-trunc) || :'
        # remove old unused images
        - 'docker rmi $(docker images -f dangling=true -q --no-trunc) || :'
        # remove old unused containers
        - 'docker volume rm $(docker volume ls -f dangling=true -q) || :'
    tags:
        - testing
        - shell
        - deploy
    only:
        - /^feature\/.*/
        - /^hotfix\/.*/

cleanup:docker:staging:
    stage: cleanup
    when: always
    script:
        # remove old stopped containers
        - 'docker rm -v $(docker ps --filter "status=exited" --filter "name=${CI_PROJECT_NAME}" -q --no-trunc) || :'
        # remove old unused images
        - 'docker rmi $(docker images -f dangling=true -q --no-trunc) || :'
        # remove old unused containers
        - 'docker volume rm $(docker volume ls -f dangling=true -q) || :'
    tags:
        - staging
        - shell
    only:
        - develop
